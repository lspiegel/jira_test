api = 2
core = 7.x
projects[drupal][version]  = "7.17"

; Modules
projects[ctools]	 		= 1.0
projects[ctools][subidr]	= "contrib"

projects[features]	 		= 1.0
projects[features][subidr]	= "contrib"

projects[services]	 		= 3.2
projects[services][subidr]	= "contrib"

projects[mollom]	 		= 2.3
projects[mollom][subidr]	= "contrib"

projects[pathauto]	 		= 1.2
projects[pathauto][subidr]	= "contrib"

projects[token]		 		= 1.0
projects[token][subidr]		= "contrib"

projects[piwik]		 		= 2.3
projects[piwik][subidr]		= "contrib"

projects[views]		 		= 3.5
projects[views][subidr]		= "contrib"

projects[webform]	 		= 3.18
projects[webform][subidr]	= "contrib"
